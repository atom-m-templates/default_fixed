// Подсветка активных пунктов меню
$(function () {
    $('.fpsMainMenu li a').each(function () {
        var location = window.location.href;
        var link = this.href;
		var result = location.match(link);
		if(location == link) {
		  $(this).addClass('active');
		}
		if(result != null) {
		  $(this).addClass('current');
		}
    });
});

// Кнопка открытия мини-чата.
function ochat() {
 if($('#ochat').css('display') == 'none') {
   $('#ochat').show();
   chat.jspAPI.reinitialise();
 }else{
   $('#ochat').hide();
 }
}

// Запуск FancyBox
$(document).ready(function() {
	$("a.gallery").fancybox();
});

// Скрытие/раскрытие дополнительных полей при добавлении материала
function metaTags(element) {
	if (!$(element).is(':checked')) {
		$('#meta').slideUp("slow");
		$('#tags').slideUp("slow");
	} else {
		$('#meta').slideDown("slow");
		$('#tags').slideDown("slow");
	}
};

//Автоподстановка имени пользователя в форме
$(function() {
    $('[list=findusers]').keyup(function() {
        if ($('[list=findusers]').val().length > 2) {
            $.get('/users/search_niks/'+$('[list=findusers]').val()+'/', {}, function(data){
                $('#findusers').html(data);
            });
        } else {
            $('#findusers').html('');
        };
    });
    return;
});

// Выводит индекс в конце текущего url(если он есть)
function intURLafter(){
    var loc = window.location.href,
         url = loc.split('/'),
		 output;
		  
	var i = 5; // начинаем с 5 чтобы меньше итераций делать(http:1/2/site.ru3/meseges4/наш id)
    while (url[i] !== undefined) { // если www_root чему то равен, то увеличиваем "глубину"
      id = url[i];
	  i++;
	}
	
	if ( /\d/.test(id) ) output = id; //если id из цифр, то выводим
	
	return output;
};

$(function() {
    $('.charcount').keyup(function(){
        maxLength = $(this).attr('maxlength');
        name = $(this).attr('name');
        if ($(this).val().length > 0 && maxLength != null) {
            $('.charcount.'+name).text('Введено ' + $(this).val().length + ' из ' + maxLength + ' символов');
        } else {
            $('.charcount.'+name).text('');
        }
    });
});

function replyComment(el, id, name){
    $('.comment_tree').removeClass('reply_comment');
    $(el).parents('.comment_tree').addClass('reply_comment');
    $('.addcomment .inftitle').html('Ответ для '+name);
    $('.addcomment input[name=reply]').val(id);
    $('.addcomment .canselReplyComment').css('display', 'inline-block');
}

function canselReplyComment(){
    $('.comment_tree').removeClass('reply_comment');
    $('.addcomment .inftitle').html('Добавление комментария');
    $('.addcomment input[name=reply]').val(0);
    $('.addcomment .canselReplyComment').css('display', 'none');
}